# md2html

Markdown 形式のファイルを HTML に変換し標準出力するツール。

テンプレート(header,footer)、CSS や GMF(GitHub Flavored Markdown), PHP-Markdown などの拡張を設定済みなのでインストールしてすぐに利用できます。

## 特徴

通常の Markdown および次の書式に対応

* 改行
* チェックリスト
  * org-mode の intermediate-state( 途中経過の[-]表示)
* コードシンタックス
* 定義リスト
* jekyll header (Front Matter)
* 絵文字
* フットノート(注釈)
* 目次(TOC)
* 自動リンク
* アサイド
* ハイライト
* フローチャート・ダイアグラム(mermaid: https://mermaidjs.github.io/)

出力結果サンプル: http://garin.jp/assets/other/markdown.html


## 必要環境

ruby-2.5.0
naruhoso ruby-3.x desu
ruby desu

* ruby-2.5.0 later
* bundler
* git (optional)

## インストール

``` console
$ git clone https://gitlab.com/garin54/md2html.git
$ cd md2html
$ bundle install
```

### オプション:絵文字の画像
assets/images/emoji/unicode に emojione の絵文字画像を配置する

FOSS 版の EmojiOne を使用しています
https://github.com/emojione/emojione/tree/2.2.7

``` console
$ rake emoji
```

### オプション:コマンド化

``` console
$ sudo ln -s `realpath bin/md2html` /usr/local/bin/md2html
```

## 使い方

``` console
// from file
$ ./bin/md2html README.md > README.html

// from stdin
$ cat README.md | ./bin/md2html > README.html
```

### コマンドラインオプション
`--embed`: CSS と画像を HTML に埋め込む
`--toc`: ToC(目次)を挿入
`--toc-if-plenty-of-headlinel Num`: ヘッドラインが Num より多い時だけ ToC(目次)を挿入

## 仕組み

文字列を Redcarpet で HTML に変換した上で、html_pipelie のフィルタをかけます

> markdown file → [ redcarpet → html_pipeline ] → html file

## 設定/拡張
lib/md2html の template, redcarpet, html_pipeline のライブラリを手動で変更してください

## なぜ作ったか?
自分の好みに合う Markdown から HTML に変換するコマンドラインツールがなかったから

* pandoc
  * ネストしたリストに4スペース以上のインデントが必要
  * http://sky-y.github.io/site-pandoc-jp/users-guide/
* jekyll
  * 絵文字、チェックリスト、フットノートなどに未対応
  * 確認するのにサーバの起動が必要
* grip
  * github の API を使うのでオフラインのときに使えない

## 対応フォーマット機能
[デフォルトの Markdown 書式](https://daringfireball.net/projects/markdown/)の他に次の機能が有効になっています

### 自動リンク

http://google.com

### テーブル(php-markdwon)

| AAA | BBB | CCC |
|-----+-----+-----|
| aaa | bbb | ccc |
| 111 | 222 | 333 |

### チェックリスト(タスクリスト)

- [X] タスク1
- [-] タスク2
  - [X] タスク3
  - [ ] タスク4

org-mode の intermediate-state([-]) にも対応 #3

### 改行
 本文 の 改行を
のまま改行(&lt;br&gt;)で表示

### 注釈;フットノート

Some text with a footnote[^1].
[^1]: The linked footnote appears at the end of the document.

### 上付き文字

this is the 2^(nd) time

### コードシンタックスハイライト(pygments)

**ruby**

`hello.rb`
``` ruby
puts "hello world"
```

**diff**

``` diff
@@ -130,2 +130,2 @@
-aaa
+bbb
```

### 絵文字
:smile:

### HTML コメント
<!-- this is comment -->
↑ここのコメントがあります↑ <!-- 行中のはコメントになりません -->

### 定義リスト

[#4](https://gitlab.com/garin54/md2html/issues/4)

Label1
: Some description of label 1.

Label2
: Some description of label 2.

※暫定対応: https://gitlab.com/garin54/md2html/merge_requests/11

※redcarpet の対応待ち: https://github.com/vmg/redcarpet/issues/108
※commonmark での議論 : https://talk.commonmark.org/t/description-list/289

### フローチャート・ダイアグラム
[mermaid](https://mermaidjs.github.io/)に対応。
コードブロックが mermaid だった時にフローチャートを自動で生成する

<pre>
&#096;&#096;&#096;mermaid
graph TD;
  A-->B;
  A-->C;
&#096;&#096;&#096;
</pre>

```mermaid
graph TD;
  A-->B;
  A-->C;
```

### jekyll header (Front Matter)

文章の先頭に Front Matter があった場合は YAML のコードブロックに変換する。

<pre>
---
author : garin54@example.com
date : 2017-01-01
update : 2017-11-06
version: 0.1.1
---

↓

``` yaml
author : garin54@example.com
date : 2017-01-01
update : 2017-11-06
version: 0.1.1
```
</pre>


## ソースコード

* https://gitlab.com/garin54/md2html

## License
See LICENSE.txt
