# **section** だけの行をセクション名として扱う
class StrongSectionFilter < HTML::Pipeline::Filter
  def call
    doc.search("p strong").each do |strong|
      strong["class"] = "section" if strong.parent.children.text == strong.text
    end
    doc
  end
end
