module Md2Html
  class Html
    def initialize(text, cmd_opts)
      @cmd_opts = cmd_opts
      @text = JekyllHeader.new(text).to_markdown
      html = RedCarpet.new(@text).to_markdown
      # Nokogiri::HTML::DocumentFragment
      @pipeline = HtmlPipeline.new(html, cmd_opts)
      @template = Template.new(@pipeline, cmd_opts)
    end

    def output
      @template.header + toc + body + @template.footer
    end

    private

    def body
      @pipeline.output.to_s
    end

    def toc
      if need_toc?
        @pipeline.toc
      else
        ""
      end
    end

    def need_toc?
      return true if @cmd_opts[:toc] == true
      return true if @cmd_opts[:plenty_of_header] && @pipeline.headers.count >= @cmd_opts[:plenty_of_header]
      nil
    end
  end
end
