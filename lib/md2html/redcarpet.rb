require 'redcarpet'
require_relative 'pygments'

module Md2Html
  class RedCarpet
    def initialize(source)
      @source = source
      @renderer = renderer
      @options  = options
    end

    def to_markdown
      Redcarpet::Markdown.new(@renderer, @options).render(@source)
    end

    private

    def renderer
      # hard_wrap: 改行を改行として扱う
      # filter_html: インプットストリング(source)の HTML を許可しない
      HTMLwithPygments.new(hard_wrap: true,
                           filter_html: false)
    end

    def options
      # fenced_code_blocks: ~~~. ``` ブロックの使用
      # underline : <i> を <u> に変換
      {tables: true,
       autolink: true,
       footnotes: true,
       no_intra_emphasis: true,
       disable_indented_code_blocks: true,
       strikethrough: true,
       lax_spacing: true,
       highlight: true,
       quote: true,
       superscript: true,
       fenced_code_blocks: true,
       underline: false}
    end
  end
end
