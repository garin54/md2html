require 'pygments'

module Md2Html
  # コードハイライト用のクラス
  class HTMLwithPygments < Redcarpet::Render::XHTML
    def block_code(code, language)
      case language
      when "mermaid"
        mermaid_block(code)
      else
        Pygments.highlight(code, lexer: language)
      end
    rescue MentosError # Pygments に language の登録がなければそのまま <pre> で出力
      "<pre>#{code}</pre>"
    end

    def mermaid_block(code)
      %(<div class="mermaid">#{code}</div>)
    end
  end
end
