module Md2Html
  # Jekyll のヘッダ表記を markdown のコードブロックに変換
  class JekyllHeader
    def initialize(source)
      @source = source
      @lines  = @source.lines
    end

    # @todo: \r\n 改行には未対応
    def to_markdown
      return @source.lines.join unless has_header?
      lines = @source.lines
      lines[0] = "``` yaml\n"
      lines[header_end_line + 1] = "```\n" + markdown_meta_title
      lines.join
    end

    private

    def header?
      @lines[0].match?(/^---$/)
    end
    alias has_header? header?

    def header_end_line
      return nil unless has_header?
      @header_end_line ||= @lines[1..@lines.size].find_index {|line| line =~ /^---$/ }
      raise "Jekyll header not end" if @header_end_line.nil?
      @header_end_line
    end

    def header_list
      @header_list ||= @lines[1..header_end_line].map {|line| line_split(line) }
        .yield_self {|arr| Hash[arr] }
      @header_list
    end

    # jekyll header の行を分割する
    def line_split(line)
      line.split(":", 2).map(&:strip)
    end

    # jekyll のヘッダ に title ヘッダがあれば
    # markdown 用のタイトル(# title)ヘッダを生成する
    # jekyll の仕様で title ヘッダがある場合は 1行目が ## header になるため
    # もし、markdown_header_title があればそちらを優先
    def markdown_meta_title
      return "" if markdown_header_title?
      header_list['title'] ? %(# #{header_list['title']}\n) : ""
    end

    # 最初のヘッダ要素が # title かどうか
    def markdown_header_title?
      first_header = @lines.find {|line| line =~ /^#+\s+/ }
      return true if /^#\s+/.match?(first_header)
    end
  end
end
