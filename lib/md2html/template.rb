module Md2Html
  class Template

    # pipeline is Nokogiri::HTML::DocumentFragment
    def initialize(pipeline, cmd_opts)
      @pipeline = pipeline
      @cmd_opts = cmd_opts
    end

    def header
<<-EOL
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>#{title}</title>
#{stylesheet}
</head>
<body>
EOL
    end

    def stylesheet
      if @cmd_opts[:embed]
<<-EOL
<style>
<!--
#{File.open(File.join(ASSETS_BASE, 'stylesheets/default.css')).read}
#{File.open(File.join(ASSETS_BASE, 'stylesheets/pygments/default.css')).read}
-->
</style>

<script>
#{File.open(File.join(ASSETS_BASE, 'javascripts/mermaid.min.js')).read}
mermaid.initialize({startOnLoad:true});
</script>
EOL
      else
<<-EOL
<link rel="stylesheet" href="#{File.join(ASSETS_BASE, 'stylesheets/default.css')}">
<link rel="stylesheet" href="#{File.join(ASSETS_BASE, 'stylesheets/pygments/default.css')}">
<script src="#{File.join(ASSETS_BASE, 'javascripts/mermaid.min.js')}"></script>
<script>mermaid.initialize({startOnLoad:true});</script>
EOL
      end
    end

    def footer
<<-EOL
<hr />
created at: #{Time.now}, generated by md2html #{Md2Html::VERSION}
</body>
</html>
EOL
    end

    def title
      @title ||= @pipeline.output.css("h1").first&.inner_text
    end
  end
end
