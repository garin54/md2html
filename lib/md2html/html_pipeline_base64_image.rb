require "base64"
require "open-uri"

# 画像を base64 に変換して img タグに埋め込む
class Base64ImageFilter < HTML::Pipeline::Filter
  def call
    doc.search("img").each do |img|
      next if img['src'].nil?
      # file スキームの時はスキーム名を削除する
      img['src'] = img['src'].gsub(/^file:\/\//, "")
      src = img['src'].strip
      begin
        binary = open(src).read
        base64 = Base64.strict_encode64(binary)
        suffix = src.split(".").last
        img["src"] = "data:image/#{suffix};base64,#{base64}"
      rescue
        img["src"] = src
      end
    end
    doc
  end
end
