# 暫定版の definition list
#
# 以下のような形式にだけ対応
#
# title
# :  definition1
# :  definition2
#
# redcarpet の対応が完了したらそちらを使う
# https://github.com/vmg/redcarpet/issues/108
#
class InterimDefinitionListFilter < HTML::Pipeline::Filter
  def call
    doc.search("p").each do |para|
      next unless para.text =~ /^.*\n:\s+.*/
      dt, *dd = para.text.split("\n")
      dd = dd.map {|s| s.gsub(/^:/, "").strip }.join("<br>\n")
      para.content = nil
      para.add_child "<dl><dt>#{dt}<dd>#{dd}"
    end
    doc
  end
end
