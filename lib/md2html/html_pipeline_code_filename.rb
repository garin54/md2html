# `file.rb` だけの行をコードブロックのファイル名として扱う
class CodeFileNameFilter < HTML::Pipeline::Filter
  def call
    doc.search("p code").each do |code|
      code["class"] = "filename" if code.parent.children.text == code.text
    end
    doc
  end
end
