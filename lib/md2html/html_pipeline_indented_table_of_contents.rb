# https://github.com/jch/html-pipeline/blob/master/lib/html/pipeline/toc_filter.rb
# Copyright (c) 2012 GitHub Inc. and Jerry Cheung
# Released under the MIT license
# https://github.com/jch/html-pipeline/blob/master/LICENSE
#
# の修正版。以下を修正
# * 目次をヘッダレベルに応じて階層化
# * ヘッダ1 をタイトル表示に
# * 目次の対象をヘッダ2、ヘッダ3、ヘッダ4 に限定
# * リストの種類を ul から ol に変更
#
HTML::Pipeline.require_dependency('escape_utils', 'TableOfContentsFilter')

module HTML
  class Pipeline

    class IndentedTableOfContentsFilter < Filter
      PUNCTUATION_REGEXP = RUBY_VERSION > '1.9' ? /[^\p{Word}\- ]/u : /[^\w\- ]/

      # The icon that will be placed next to an anchored rendered markdown header
      def anchor_icon
        context[:anchor_icon] || '<span aria-hidden="true" class="octicon octicon-link"></span>'
      end

      def call
        base_level = 2
        level = prev_level = base_level
        result[:toc] = ''
        headers = Hash.new(0)

        doc.css('h2, h3, h4').each do |node|
          level = node.name[/[0-9]/].to_i
          text = node.text
          id = ascii_downcase(text)
          id.gsub!(PUNCTUATION_REGEXP, '') # remove punctuation
          id.tr!(' ', '-') # replace spaces with dash

          uniq = headers[id] > 0 ? "-#{headers[id]}" : ''
          headers[id] += 1
          if header_content = node.children.first
            case prev_level <=> level
            when -1
              result[:toc] << %(<ol class="toc">\n) * (level - prev_level)
            when 1
              result[:toc] << "</ol>\n" * (prev_level - level)
            end
            result[:toc] << %(<li><a href="##{id}#{uniq}">#{EscapeUtils.escape_html(text)}</a></li>\n)

            header_content.add_previous_sibling(%(<a id="#{id}#{uniq}" class="anchor" href="##{id}#{uniq}" aria-hidden="true">#{anchor_icon}</a>))
          end
          prev_level = level
        end

        result[:toc] << "</ol>\n" * ( level - base_level) if level > 1
        result[:toc] = %(<ol class="toc">\n#{result[:toc]}</ol>\n\n) unless result[:toc].empty?
        doc.css('h1').each do |node|
          result[:toc] = %(<h1 class="toc">#{node.text}</h1>\n) + result[:toc]
        end
        doc
      end

      if RUBY_VERSION >= '2.4'
        def ascii_downcase(str)
          str.downcase(:ascii)
        end
      else
        def ascii_downcase(str)
          str.downcase
        end
      end
    end
  end
end
