require 'html/pipeline'
require 'html/pipeline/youtube'
require_relative 'task_list/filter'
require_relative "html_pipeline_base64_image"
require_relative "html_pipeline_code_filename"
require_relative "html_pipeline_strong_section"
require_relative "html_pipeline_interim_definition_list"
require_relative "html_pipeline_indented_table_of_contents"

module Md2Html
  class HtmlPipeline
    def initialize(source, cmd_opts)
      @source = source
      @cmd_opts = cmd_opts
      @pipeline = pipeline
    end

    def output
      @pipeline.call(@source)[:output]
    end

    def toc
      @pipeline.call(@source)[:toc]
    end

    def headers
      @pipeline.call(@source)[:toc].gsub("<li>")
    end


    private

    def pipeline
      context = {asset_root: File.join(ASSETS_BASE, "images")}
      pipelines = [HTML::Pipeline::YoutubeFilter,
                   HTML::Pipeline::EmojiFilter,
                   TaskList::Filter,
                   HTML::Pipeline::IndentedTableOfContentsFilter]
      pipelines += [Base64ImageFilter] if @cmd_opts[:embed]
      pipelines += [CodeFileNameFilter]
      pipelines += [StrongSectionFilter]
      pipelines += [InterimDefinitionListFilter]

      HTML::Pipeline.new pipelines, context
    end
  end
end
